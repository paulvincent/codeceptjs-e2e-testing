const merge = require("lodash.merge");
const { config: commonConfig } = require("./codecept.conf");

const specificConfig = {
  helpers: {
    WebDriver: {
      host: process.env.SELENIUM_STANDALONE_CHROME_PORT_4444_TCP_ADDR,
      protocol: "http",
      port: 4444,
    },
  },
};

exports.config = merge(commonConfig, specificConfig);
